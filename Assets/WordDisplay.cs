using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WordDisplay : MonoBehaviour
{ [SerializeField]
    public Transform Pesawat;
    public Transform nembak;
    public Transform laser;
    public Transform explo;
    public Transform blackhole1;
    public Vector2 pos;
    WordManager _wordmanager;
    public Text text;
    public float kecepatanjatuh = 1f;
    public float kecepatanjatuh1 = 0.1f;
    private bool cek = false;
    private float jarak;
    public scoremanager score;
    public void Start()
    {
        Pesawat = GameObject.Find("pesawat").GetComponent<Transform>();
        nembak = GameObject.Find("misile").GetComponent<Transform>();
        laser = GameObject.Find("laser").GetComponent<Transform>();
        explo = GameObject.Find("pngwing.com_0").GetComponent<Transform>();
        score = GameObject.Find("Canvas").GetComponent<scoremanager>();
        blackhole1 = GameObject.Find("bh_0").GetComponent<Transform>();
    }
    public void Setword (string word)
    {
        text.text = word;
    }
    public void HapusTulisan ()
    {
        soundmanager.playsound("tembak");
        score.tambahscore();
        cek = true;
        text.text = text.text.Remove(0, 1);
        text.color = Color.yellow;
        
    }
    public void HapusKata()
    {

        score.tambahscore();
            cek = false;
            nembak.position = new Vector2(0, -3.77f);
            Destroy(gameObject);
        
    }
    public void Update()

    {
        jarak = Vector2.Distance(nembak.position, transform.position);
        transform.position = Vector2.MoveTowards(gameObject.transform.position, Pesawat.position, kecepatanjatuh * Time.deltaTime);
        if (cek)
        {
            if (jarak != 0)
            {
                
                nembak.position = Vector2.MoveTowards(nembak.position, transform.position, 10f * Time.deltaTime);
            }
            else
            {
                cek = false;
                
                nembak.position = new Vector2(0, -3.77f);
            }
            Vector3 arah = transform.position - laser.position;
            float angle = Mathf.Atan2(arah.y, arah.x) * Mathf.Rad2Deg;
            
            laser.localRotation = Quaternion.Euler(0f, 0f, angle);
            explo.position = transform.position;
            blackhole1.position = transform.position;
        }

        
    }
}
