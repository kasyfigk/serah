using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGenerator : MonoBehaviour
{
    private static string[] wordlist =
    { "mechanics","Thermodynamics","Electromagnetism","Quantum","atomic","Optics","molecula","Relativistic","particle","Mesoscopic","Cosmology","Relativity","waves","geographers" };
    public static string GetRandomWord ()
    {
        int randomIndex = Random.Range(0, wordlist.Length);
        string randomWord = wordlist[randomIndex];
        return randomWord;
    }
}
