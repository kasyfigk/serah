using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordManager : MonoBehaviour
{
    [SerializeField]
    private bool hasActiveWord;
    public Transform misil;
    private Word activeWord;
    public List<Word> words;
    public WordSpawner wordSpawner;
    lasermanager manager;
    blackhole black;
    public Word kata;
    public WordTimer timi;
    public nabrak nambrak;
    
    ledakan meledak;
    public WordDisplay display;
    public Text batasnembak;
    public int ceknembak;
    public int batas2;
    public int levell;
    public int batasspwn;
    public Text level;
    public bool cokcok;
    private float fitur;


    private void Start()
    {
        timi = GetComponent<WordTimer>();
        batas2 = 1;
        nambrak =  GameObject.Find("pesawat").GetComponent<nabrak>();
    }
    public void AddWord()
    {

        Word word = new Word(WordGenerator.GetRandomWord(), wordSpawner.SpawnWord());
        //Debug.Log(word.word);
        
        words.Add(word);


    }
    public void TypeLetter (char letter)
    { 
        
        if (hasActiveWord)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            { if (ceknembak != 10) {
                    ceknembak++;
                    soundmanager.playsound("lazer");
                    activeWord.WordTyped1();
                    manager = GameObject.Find("wordManager").GetComponent<lasermanager>();
                    manager.setlaser();
                    
                }
            }
            
            else if (activeWord.GetNextletter() == letter)
            {
                fitur = Random.RandomRange(1, 100);
                if (fitur <= 30)
                {
                    soundmanager.playsound("blckh"); 
                    activeWord.WordTyped1();
                    black = GameObject.Find("wordManager").GetComponent<blackhole>();
                    black.setblackhole();
                }
                else
                {
                    activeWord.TypeLetter();
                }
            }
            
        }
        else 
        { 
            foreach(Word word in words)
            {
                if (word.GetNextletter() == letter)
                {
                    activeWord = word;
                    hasActiveWord = true;
                    word.TypeLetter();
                    
                    break;
                }
            }
        }
        if (hasActiveWord && activeWord.WordTyped())
        {
            soundmanager.playsound("duar");
            batas2++;
            hasActiveWord = false;
            words.Remove(activeWord);
            
        }

        if (batas2 >= ((timi.level + 1) * 2))
        {
            
            timi.level++;
            timi.batasspawn = 1;
            batas2 = 1;

        }
        
    }
    public void Update()
    {
        //batasspwn = timi.batasspawn;
        //levell = timi.level;
        batasnembak.text = "max launch lazer"+"\n"+ceknembak.ToString()+" | 10";
        
        level.text = "level" + "\n" + timi.level.ToString();
        cokcok = nambrak.cok;
        if (cokcok)
        {
            words.Clear();
            hasActiveWord = false;
        }
        
    }
   
    

}
