using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordSpawner : MonoBehaviour
{
    public GameObject wordPrefab;
    public Transform wordCanvas;
    // Start is called before the first frame update
    public WordDisplay SpawnWord()
    {

        Vector3 RandomPosisi = new Vector3(Random.Range(-2f, 2f), 7f);
        GameObject wordObj = Instantiate(wordPrefab, RandomPosisi, Quaternion.identity, wordCanvas);
        WordDisplay wordDisplay = wordObj.GetComponent<WordDisplay>();
        return wordDisplay;
       
    }
}
    
