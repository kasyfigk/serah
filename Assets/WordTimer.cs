using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordTimer : MonoBehaviour
{
    public WordManager wordManager;
    public float delaykata = 2f;
    private float timerkataselanjutnya = 0f;
    public int level;
    public int batasspawn=0;
    private void Start()
    {
        level = 1;
        batasspawn = 1;
    }
    private void Update()
    {
        
        if (Time.time >= timerkataselanjutnya)
        {
            if (batasspawn <= ((level + 1) * 2))
            {
                batasspawn++;
                wordManager.AddWord();
               
                timerkataselanjutnya = Time.time + delaykata;
                delaykata *= .99f;
                
            }
            

        }
    }  
}
