using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgm : MonoBehaviour
{
    private static bgm musik;



    private void Awake()
    {
        if (musik == null)
        {
            musik = this;
            DontDestroyOnLoad(musik.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
