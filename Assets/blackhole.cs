using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackhole : MonoBehaviour
{
    public float showspriteb;
    public SpriteRenderer blackhhole;
    // Start is called before the first frame update
    public void setblackhole()
    {
        showspriteb = 0.3f;
    }

    // Update is called once per frame
    public void Update()
    {
        if (showspriteb > 0)
        {

            blackhhole.enabled = true;


        }
        else
        {
            blackhhole.enabled = false;
        }
        showspriteb -= Time.deltaTime;
    }
}
