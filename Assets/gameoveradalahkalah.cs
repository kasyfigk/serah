using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameoveradalahkalah : MonoBehaviour
{
    public Text game;
    // Start is called before the first frame update
    void Start()
    {
        int scor = PlayerPrefs.GetInt("sscoresementara");
        int highscor = PlayerPrefs.GetInt("Highscore");
        game.text = "Game Over" + "\n" + "Score: " + scor.ToString() + "\n" + "HighScore: " + highscor.ToString();
    }

    // Update is called once per frame
    public void res()
    {
        SceneManager.LoadScene("main");
    }
    public void hom()
    {
        SceneManager.LoadScene("menu");
    }
}
