using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lasermanager : MonoBehaviour
{
    
    public float showsprite;
    public SpriteRenderer laser;
    // Start is called before the first frame update
    public void setlaser()
    {
        showsprite = 0.3f;
    }

    // Update is called once per frame
    public void Update()
    {
        if (showsprite > 0)
        {

            laser.enabled = true;

            
        }
        else
        {
            laser.enabled = false;
        }
        showsprite -= Time.deltaTime;
    }
}
