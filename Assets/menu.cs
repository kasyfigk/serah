using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : MonoBehaviour
{
     
    public Text apajah;
    public void Start()
    {
        apajah.text = "HighScore: "+ PlayerPrefs.GetInt("Highscore");
    }
    public void playmain()
    {
        SceneManager.LoadScene("main");
    }
    public void quite()
    {
        Application.Quit();
    }
}
