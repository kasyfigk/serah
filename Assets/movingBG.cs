using UnityEngine;

public class movingBG : MonoBehaviour
{
    [Range(-1f,1f)]
    public float movingspeed = 0.5f;
    private float offset;
    private Material mat;

    void Start()
    {
        mat = GetComponent<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        offset += (Time.deltaTime * movingspeed) / 10f;
        //Debug.Log(offset);
        mat.SetTextureOffset("_MainTex", new Vector2 (0,offset ));
    }
}
