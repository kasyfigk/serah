using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoremanager : MonoBehaviour
{
    public Text score;
    public Text highscore;
    public int score1;
    public int highscore1;
    // Start is called before the first frame update
    void Start()
    {
        highscore1 = PlayerPrefs.GetInt("Highscore", 0);
        score.text = "Score :" + score1.ToString();
        highscore.text = "Highscore :" + highscore1.ToString();

    }

    // Update is called once per frame
    public void tambahscore()
    {
        score1 += 100;
        score.text = "Score :" + score1.ToString();
        if (highscore1 < score1)
        {
            PlayerPrefs.SetInt("Highscore", score1);
        }
    }
}
