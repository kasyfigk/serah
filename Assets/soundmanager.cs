using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class soundmanager : MonoBehaviour
{
    [SerializeField] Image iconon;
    [SerializeField] Image iconof;
    public static AudioClip bomb, launchattack, UImenuclick, UIgameclik,lazerlaunch,blackh;
    static AudioSource audioSrc;
    private bool muted = false;




    // Start is called before the first frame update
    public void Start()
    {
        bomb = Resources.Load<AudioClip>("ledak");
        launchattack = Resources.Load<AudioClip>("launch");
        UImenuclick = Resources.Load<AudioClip>("ui_menu_sound");
        UIgameclik = Resources.Load<AudioClip>("BUTTON_Plastic_Light_Switch_Off_mono");
        lazerlaunch = Resources.Load<AudioClip>("lazer");
        blackh = Resources.Load<AudioClip>("bhsound");
        audioSrc = GetComponent<AudioSource>();

        if (!PlayerPrefs.HasKey("muted"))
        {
            PlayerPrefs.SetInt("muted", 0);
            loadmute();
        }
        else
        {
            loadmute();
        }

        henshinicon();
        AudioListener.pause = muted;
    }

    // Update is called once per frame
    public static void playsound(string clip)
    {

        switch (clip)
        {

            case "duar":
                audioSrc.PlayOneShot(bomb);
                break;

            case "tembak":
                audioSrc.PlayOneShot(launchattack);
                break;
            case "lazer":
                audioSrc.PlayOneShot(lazerlaunch);
                break;
            case "blckh":
                audioSrc.PlayOneShot(blackh);
                break;

            case "UIMenu":
                audioSrc.PlayOneShot(UImenuclick);
                break;

            case "UIMenu1":
                audioSrc.PlayOneShot(UIgameclik);
                break;

        }
    }
    public void tomboltekan()
    {
        if (muted == false)
        {
            muted = true;
            AudioListener.pause = true;
        }
        else
        {
            muted = false;
            AudioListener.pause = false;
        }
        savemute();
        henshinicon();
    }
    private void loadmute()
    {
        muted = PlayerPrefs.GetInt("muted") == 1;
    }
    private void savemute()
    {
        PlayerPrefs.SetInt("muted", muted ? 1 : 0);
    }
    private void henshinicon()
    {
        if (muted == false)
        {
            iconon.enabled = true;
            iconof.enabled = false;
        }
        else
        {
            iconon.enabled = false;
            iconof.enabled = true;
        }
    }
}