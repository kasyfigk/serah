using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class tombol : MonoBehaviour
{
    public bool pause;
    public void homeless()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("menu");
        
    }
    public void paused ()
    {
        if (!pause)
        {
            Time.timeScale = 0f;
            pause = true;
        }
        else
        {
            Time.timeScale = 1f;
            pause = false;

        }
    }
}
